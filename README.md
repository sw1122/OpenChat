###欢迎来到OpenChat项目
这是一个仿微信的即时通信软件，使用[微客服SDK](http://www.appkefu.com/)来进行通信

本项目使用Eclipse IDE，SDK版本为4.3，编码采用GBK

####功能：
* 登录和注册功能
* 输入用户名或扫二维码添加好友功能
* 更改个人信息功能（包括头像、昵称和个性签名）
* 聊天功能，可以发送文字、表情、语音、图片和视频
* 增加图灵智能机器人

####使用的第三方库：
* [微客服IM SDK](http://www.appkefu.com/imsdk.html)：实现用户之间的聊天和信息管理
* [zxing](https://github.com/zxing/zxing)：实现生成二维码和扫描二维码功能
* [Android-Universal-Image-Loader](https://github.com/nostra13/Android-Universal-Image-Loader)：实现图片异步加载
* [PhotoView](https://github.com/chrisbanes/PhotoView)：支持双击或双指缩放的ImageView
* [LitePal](https://github.com/LitePalFramework/LitePal)：开源的Android数据库框架，采用了对象关系映射(ORM)的模式
* [EventBus](https://github.com/greenrobot/EventBus)：针对Android优化的发布/订阅事件总线
* [Crouton](https://github.com/keyboardsurfer/Crouton)：丰富样式的Toast
