package com.weicong.openchat.video;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import android.os.Environment;
import android.util.Base64;

public class VideoUtil {
	public static final String VIDEO_TAG = "!#sdfdfwwwefsdss!#";
	
	public static String getVideoSendMsg(String filePath) {
		try {
			File file = new File(filePath);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			byte[] data = new byte[1024];
			FileInputStream fis = new FileInputStream(file); 
			int len = 0;
			while((len = fis.read(data)) != -1) {               
				byteArrayOutputStream.write(data, 0, len);           
			}       
			String content = Base64.encodeToString(byteArrayOutputStream.toByteArray(), 
					Base64.DEFAULT);
			fis.close();
			byteArrayOutputStream.close();
			StringBuilder sb = new StringBuilder();
			sb.append(content);
			sb.append(VIDEO_TAG);
			return sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getVideoMsg(String filePath) {
		return  filePath + VIDEO_TAG;
	}
	
	public static String getVideoContentPath(String msg) {
		int end = msg.lastIndexOf(VIDEO_TAG);
		return msg.substring(0, end);
	}
	
	public static String saveVideo(String msg) {
		int end = msg.lastIndexOf(VIDEO_TAG);
		String content = msg.substring(0, end);
		byte[] data = Base64.decode(content, Base64.DEFAULT);
		String dir = Environment.getExternalStorageDirectory()
				+ "/openchat/video";
		File fileDir = new File(dir);
		if (!fileDir.exists())
			fileDir.mkdirs();
		String fileName = generateFileName();
		File file = new File(fileDir, fileName);
		try {
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(data);
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		msg = file.getAbsolutePath() + VIDEO_TAG;
		return msg;
	}
	
	/**
	 * 随机生成文件名称
	 * 
	 * @return
	 */
	private static String generateFileName() {
		return UUID.randomUUID().toString() + ".mp4";
	}
}
