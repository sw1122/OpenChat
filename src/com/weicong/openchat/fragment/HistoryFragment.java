package com.weicong.openchat.fragment;

import java.util.List;

import org.jivesoftware.smack.util.StringUtils;
import org.litepal.crud.DataSupport;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.appkefu.lib.db.KFConversationHelper;
import com.appkefu.lib.interfaces.KFIMInterfaces;
import com.appkefu.lib.service.KFMainService;
import com.appkefu.lib.service.KFSettingsManager;
import com.appkefu.lib.ui.entity.KFConversationEntity;
import com.appkefu.lib.utils.KFImageUtils;
import com.appkefu.lib.utils.KFUtils;
import com.appkefu.lib.xmpp.XmppVCard;
import com.weicong.openchat.R;
import com.weicong.openchat.activity.ChatActivity;
import com.weicong.openchat.activity.NotificationActivity;
import com.weicong.openchat.entity.ConversationEntity;
import com.weicong.openchat.entity.MessageEntity;
import com.weicong.openchat.face.FaceUtil;
import com.weicong.openchat.utils.ImageUtil;
import com.weicong.openchat.video.VideoUtil;
import com.weicong.openchat.voice.VoiceUtil;
import com.ypy.eventbus.EventBus;

public class HistoryFragment extends Fragment {
	
	private ListView mListView;
	private List<ConversationEntity> mConversationList;
	private HistoryAdapter mHistoryAdapter;
	
	private String mMyUsername;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		EventBus.getDefault().register(this); 
		
		mMyUsername = KFSettingsManager.getSettingsManager(getActivity()).getUsername();
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_history, null);
		
		mListView = (ListView) v.findViewById(R.id.listView);
		
		TextView emptyView = new TextView(getActivity());  
		emptyView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));  
		emptyView.setText("会话记录为空");
		emptyView.setTextColor(getActivity().getResources().getColor(R.color.text_color));
		emptyView.setGravity(Gravity.CENTER);  
		emptyView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
		emptyView.setVisibility(View.GONE);  
		((ViewGroup)mListView.getParent()).addView(emptyView);  
		mListView.setEmptyView(emptyView);  
		
		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				ConversationEntity entity = mConversationList.get(position);
				if (entity.getType() == 1) {
					Intent intent = new Intent(getActivity(), NotificationActivity.class);
					intent.putExtra(NotificationActivity.USERNAME, entity.getMessage());
					intent.putExtra(NotificationActivity.TIME, entity.getDate());
					startActivity(intent);
				} else {
					Intent intent = new Intent(getActivity(), ChatActivity.class);
					intent.putExtra(ChatActivity.USERNAME, entity.getUsername());
					startActivity(intent);
				}
			}
		});
		
		registerForContextMenu(mListView);
		
		return v;
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		getActivity().getMenuInflater().inflate(R.menu.delete_context_menu, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		int position = info.position;
		
		switch (item.getItemId()) {
		case R.id.action_delete:
			ConversationEntity entity = mConversationList.remove(position);
			DataSupport.delete(ConversationEntity.class, entity.getId());
			DataSupport.deleteAll(MessageEntity.class, "username = ?", entity.getUsername());
			mHistoryAdapter.notifyDataSetChanged();
			return true;
		}
		
		return super.onContextItemSelected(item);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		IntentFilter intentFilter = new IntentFilter();
        //监听消息
        intentFilter.addAction(KFMainService.ACTION_XMPP_MESSAGE_RECEIVED);
        getActivity().registerReceiver(mXmppreceiver, intentFilter); 
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		mConversationList = DataSupport.where("belong = ? and username != ?", mMyUsername, mMyUsername).find(ConversationEntity.class);
		mHistoryAdapter = new HistoryAdapter(mConversationList);
		mListView.setAdapter(mHistoryAdapter);
		mHistoryAdapter.notifyDataSetChanged();
		invalidateConversation();
	}
	
	@Override
	public void onStop() {
		super.onStop();
		getActivity().unregisterReceiver(mXmppreceiver);
	}
	
	private BroadcastReceiver mXmppreceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals(KFMainService.ACTION_XMPP_MESSAGE_RECEIVED)) {
            	String body = intent.getStringExtra("body");
            	String from = StringUtils.parseName(intent.getStringExtra("from"));
            	
            	if (body.contains(VoiceUtil.VOICE_TAG)) {
            		body = VoiceUtil.saveVoice(body);
            	} else if (body.contains(VideoUtil.VIDEO_TAG)) {
            		body = VideoUtil.saveVideo(body);
            	} else if (body.contains(ImageUtil.IMAGE_TAG)) {
            		body = ImageUtil.saveImage(body);
            	}
            	
            	MessageEntity entity = new MessageEntity();
            	entity.setContent(body);
            	entity.setUsername(from);
            	entity.setDate(KFUtils.getDate());
            	entity.setType(0);
            	entity.save();
            	
            	update(entity);
            	
            	invalidateConversation();
            }
        }
    };
	
    private void update(MessageEntity entity) {
		
		int i;
		ConversationEntity CEntity;
		for (i = 0; i < mConversationList.size(); i++) {
			CEntity = mConversationList.get(i);
			if (CEntity.getUsername().equals(entity.getUsername())) {
				break;
			}
		}
		
		if (i < mConversationList.size()) {
			CEntity = mConversationList.get(i);
			CEntity.setCount(CEntity.getCount()+1);
			CEntity.setDate(entity.getDate());
			CEntity.setMessage(entity.getContent());
			CEntity.update(CEntity.getId());
		} else {
			CEntity = new ConversationEntity();
			CEntity.setBelong(mMyUsername);
			CEntity.setUsername(entity.getUsername());
			CEntity.setDate(entity.getDate());
			CEntity.setCount(1);
			CEntity.setMessage(entity.getContent());
			CEntity.setType(0);
			CEntity.save();
			
			mConversationList.add(CEntity);
		}
    	
		mHistoryAdapter.notifyDataSetChanged();
    }
    
	public void onEventMainThread(MessageEntity entity) {
		update(entity);
	}
	
	private class HistoryAdapter extends ArrayAdapter<ConversationEntity> {
		
		public HistoryAdapter(List<ConversationEntity> list) {
			super(getActivity(), 0, list);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder = null;
			if (convertView == null) {
				convertView = getActivity().getLayoutInflater().inflate(R.layout.history_list_item, null);
				viewHolder = new ViewHolder();
				viewHolder.image = (ImageView) convertView.findViewById(R.id.iv_head);
				viewHolder.name = (TextView) convertView.findViewById(R.id.tv_name);
				viewHolder.message = (TextView) convertView.findViewById(R.id.tv_message);
				viewHolder.time = (TextView) convertView.findViewById(R.id.tv_time);
				
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			
			ConversationEntity entity = getItem(position);
			
			String msg = entity.getMessage();
			//判断消息是否包含表情
			if (FaceUtil.contain(entity.getMessage())) {
				msg = "[表情]";
			} else if (msg.contains(VoiceUtil.VOICE_TAG)) {
				msg = "[语音]";
			} else if (msg.contains(VideoUtil.VIDEO_TAG)) {
				msg = "[视频]";
			} else if (msg.contains(ImageUtil.IMAGE_TAG)) {
				msg = "[图片]";
			}
			viewHolder.message.setText(msg);
			viewHolder.time.setText(entity.getDate());
			
			if (entity.getType() == 1) {
				viewHolder.name.setText("验证消息");
				String s = entity.getMessage() + "请求添加好友";
				viewHolder.message.setText(s);
				viewHolder.image.setImageResource(R.drawable.add_notification);
			} else {
				//首先获取昵称，如果昵称未设置，则显示对方用户名
				String nickname = KFIMInterfaces.getOtherNickname(entity.getUsername());
				if(nickname == null) {
					nickname = entity.getUsername();
				}
				viewHolder.name.setText(nickname);
				
				if(entity.getType() == 0 && !entity.getUsername().equals("")) { 
					updateVCardThread(entity.getUsername(), viewHolder.image);
				} else {
					viewHolder.image.setImageResource(R.drawable.head);
				}
			}
			
			return convertView;
		}
	}
	
	private class ViewHolder {
		public ImageView image;
		public TextView name;
		public TextView message;
		public TextView time;
	}
	
	@SuppressWarnings("unchecked")
	private void invalidateConversation() {
		ConversationEntity CEntity;
		for (KFConversationEntity entity : (List<KFConversationEntity>)
				KFConversationHelper.getConversationHelper(getActivity()).getAllConversation()) {
			//验证消息
			if (entity.getName().equals("appkefu_subscribe_notification_message")) {
				
				CEntity = new ConversationEntity();
				CEntity.setBelong(mMyUsername);
				CEntity.setUsername(entity.getName());
				CEntity.setDate(KFUtils.getDate());
				CEntity.setCount(1);
				CEntity.setMessage(entity.getMsg());
				CEntity.setType(1);
				CEntity.save();
            	
            	mConversationList.add(CEntity);
            	KFConversationHelper.getConversationHelper(getActivity().getApplicationContext())
            		.deleteConversation(entity.getName());
			}
			
		}
		
		mHistoryAdapter.notifyDataSetChanged();
	}
	
	
	@SuppressLint("HandlerLeak")
	private void updateVCardThread(final String username, final ImageView avatar) {
		
		final Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				if(msg.what == 1) {
					String avatarPath = (String)msg.obj;
					Bitmap bitmap = KFImageUtils.getBitmapByPath(avatarPath);
					if(bitmap != null)
						avatar.setImageBitmap(bitmap);
					else
						avatar.setImageResource(R.drawable.head);
				}
				
			}
		};
		
		new Thread() {
			public void run() {
				Message msg = new Message();			
				
				String avatarPath = XmppVCard.getOthersAvatarPath(username);
		
				msg.what = 1; 
				msg.obj = avatarPath; 
								
				handler.sendMessage(msg);
			}
		}.start();
		
	}
}
