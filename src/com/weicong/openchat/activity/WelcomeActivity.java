package com.weicong.openchat.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.weicong.openchat.R;
import com.weicong.openchat.utils.ToolUtil;

public class WelcomeActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);
		ToolUtil.pushActivity(this);
	}
	
	@Override
	protected void onDestroy() {
		ToolUtil.removeActivity(this);
		super.onDestroy();
	}
	
	public void onClick(View v) {
		if (v.getId() == R.id.btn_login) {
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
		} else if (v.getId() == R.id.btn_register) {
			Intent intent = new Intent(this, RegisterActivity.class);
			startActivity(intent);
		}
	}
}
