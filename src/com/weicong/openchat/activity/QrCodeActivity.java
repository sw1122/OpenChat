package com.weicong.openchat.activity;

import java.util.Hashtable;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.appkefu.lib.service.KFSettingsManager;
import com.appkefu.lib.utils.KFImageUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.weicong.openchat.R;

public class QrCodeActivity extends Activity {

	public static final String PATH = "path";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_qr_code);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("二维码名片");
		
		ImageView headView = (ImageView) findViewById(R.id.iv_image);
		String path = getIntent().getStringExtra(PATH);
		Bitmap bitmap = KFImageUtils.loadImgThumbnail(path, 100, 100);
		if(bitmap != null)
			headView.setImageBitmap(bitmap);
		
		String username = KFSettingsManager.getSettingsManager(this).getUsername();
		TextView usernameText = (TextView) findViewById(R.id.tv_username);
		usernameText.setText(username);
		
		ImageView qrCodeImage = (ImageView) findViewById(R.id.iv_qr_code);
		try {
			bitmap = makeQRImage(username, 600, 600);
			qrCodeImage.setImageBitmap(bitmap);
		} catch (WriterException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * 根据指定内容生成自定义宽高的二维码图片 
	 * @param content 需要生成二维码的内容
	 * @param width 二维码宽度
	 * @param height 二维码高度
	 * @throws WriterException 生成二维码异常
	 */
	private Bitmap makeQRImage(String content, int width, int height) throws WriterException {

		Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		// 图像数据转换，使用了矩阵转换
		BitMatrix bitMatrix = new QRCodeWriter().encode(content,
				BarcodeFormat.QR_CODE, width, height, hints);
		int[] pixels = new int[width * height];
		// 按照二维码的算法，逐个生成二维码的图片，两个for循环是图片横列扫描的结果
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (bitMatrix.get(x, y))
					pixels[y * width + x] = 0xff000000;
				else
					pixels[y * width + x] = 0x00000000;
			}
		}
		// 生成二维码图片的格式，使用ARGB_8888
		Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
		
		return bitmap;
	}
}
