package com.weicong.openchat.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.appkefu.lib.interfaces.KFIMInterfaces;
import com.appkefu.lib.service.KFMainService;
import com.appkefu.lib.service.KFSettingsManager;
import com.appkefu.lib.service.KFXmppManager;
import com.appkefu.lib.utils.KFSLog;
import com.weicong.openchat.R;
import com.weicong.openchat.utils.ToolUtil;

public class GuideActivity extends Activity {

	private KFSettingsManager mSettingsMgr;
	private Handler mHandler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ToolUtil.pushActivity(this);
		
		if (KFIMInterfaces.isLogin()) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
			finish();
		} else {
			setContentView(R.layout.activity_guide);
			mSettingsMgr = KFSettingsManager.getSettingsManager(this);
		
			mHandler = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					Intent intent = new Intent(GuideActivity.this, WelcomeActivity.class);
					startActivity(intent);
				}
			};
		
		
			login();
		}

	}
	
	@Override
	protected void onDestroy() {
		ToolUtil.removeActivity(this);
		super.onDestroy();
	}
	
	/**
	 * 登录
	 */
	private void login() {
		//检查 用户名/密码 是否已经设置,如果已经设置，则登录
		if(!"".equals(mSettingsMgr.getUsername()) && !"".equals(mSettingsMgr.getPassword())) {
			KFIMInterfaces.login(this, mSettingsMgr.getUsername(), mSettingsMgr.getPassword());
		} else {
			mHandler.sendEmptyMessageDelayed(0, 1500);
		}
    }
	
	@Override
	protected void onStart() {
		super.onStart();
		
		IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(KFMainService.ACTION_XMPP_CONNECTION_CHANGED);
        registerReceiver(mXmppreceiver, intentFilter);        

	}
	
	@Override
	protected void onStop() {
		super.onStop();

        unregisterReceiver(mXmppreceiver);
	}
	
	private BroadcastReceiver mXmppreceiver = new BroadcastReceiver() 
	{
        public void onReceive(Context context, Intent intent) 
        {
            String action = intent.getAction();
            if (action.equals(KFMainService.ACTION_XMPP_CONNECTION_CHANGED)) 
            {
                updateStatus(intent.getIntExtra("new_state", 0));        
            }
           
        }
    };
    
    private void updateStatus(int status) {
        switch (status) {
            case KFXmppManager.CONNECTED:
            	KFSLog.d("登录成功");
            	Intent intent = new Intent(this, MainActivity.class);
            	startActivity(intent);
            	finish();
                break;
            case KFXmppManager.DISCONNECTED:
            	KFSLog.d("未登录");  
                break;
            case KFXmppManager.CONNECTING:
            	KFSLog.d("登录中");
            	break;
            case KFXmppManager.DISCONNECTING:
            	KFSLog.d("登出中");
            	intent = new Intent(this, WelcomeActivity.class);
            	startActivity(intent);
            	finish();
                break;
            case KFXmppManager.WAITING_TO_CONNECT:
            case KFXmppManager.WAITING_FOR_NETWORK:
            	KFSLog.d("waiting to connect");
                break;
            default:
                throw new IllegalStateException();
        }      
    }
}
