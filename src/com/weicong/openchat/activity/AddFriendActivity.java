package com.weicong.openchat.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.appkefu.lib.interfaces.KFIMInterfaces;
import com.weicong.openchat.R;

public class AddFriendActivity extends Activity {

	private EditText mUsername;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_friend);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("添加朋友");
		
		mUsername = (EditText) findViewById(R.id.et_username);
		
		Button button = (Button) findViewById(R.id.btn_add);
		button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				addFriend();
			}
		});
	}
	
	@Override
	protected void onDestroy() {
//		Crouton.clearCroutonsForActivity(this);
		super.onDestroy();
	}
	
	public void addFriend() {
		String friendUsername = mUsername.getText().toString();
		if ("".equals(friendUsername)) {
			mUsername.setError("请输入对方用户名");
		} else if(friendUsername.contains("@")) {
			mUsername.setError("含有非法字符");
		} else {
//			//判断用户是否存在
//			if(KFIMInterfaces.isUserExist(friendUsername)) {
//				//添加好友
//				KFIMInterfaces.addFriend(this, friendUsername, "我是昵称");
//			} else {
//				Crouton.makeText(this, "用户不存在", Style.CONFIRM).show();
//			}
			KFIMInterfaces.addFriend(this, friendUsername, "");
			finish();
		}  
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
