package com.weicong.openchat.activity;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.weicong.openchat.R;
import com.weicong.openchat.fragment.ProfileFragment.Event;
import com.weicong.openchat.view.ClipView;
import com.ypy.eventbus.EventBus;

public class ClipPictureActivity extends Activity implements OnTouchListener {

	public static final String PATH = "path";
	
	// 定义3种模式
	private static final int NONE = 0; 
	private static final int DRAG = 1; //移动
	private static final int ZOOM = 2; //缩放
	
	private int mCurrentMode = NONE; //记录当前的模式
	
	private PointF mStart = new PointF(); //记录起初的位置
	private PointF mMid = new PointF(); //记录中点的位置
	private float mOldDistance = 0f; //保存一开始两点间的距离
	
	private Matrix mCurrentMatrix = new Matrix(); //当前矩阵
	private Matrix mSavedMatrix = new Matrix(); //保存矩阵
	
	private int mWindowWidth, mWindowHeight;
	
	private ImageView mSrcPictureView;
	
	private ClipView mClipView;
	
	private int mActionBarHeight = 0; //操作栏的高度
	private int mStatusBarHeight = 0; //状态栏的高度
	
	private Bitmap mBitmap;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_clip_picture);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle("裁剪");

		mClipView = (ClipView) findViewById(R.id.clip_view);
		mSrcPictureView = (ImageView) findViewById(R.id.iv_head);
		mSrcPictureView.setOnTouchListener(this);
		
		String path = getIntent().getStringExtra(PATH);
		//在装入内存前对其进行采样以节约内存
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;
		mBitmap = BitmapFactory.decodeFile(path, options);
		int boundWidth = mClipView.getBoundWidth();
		mBitmap = getBitmap(mBitmap, boundWidth, boundWidth);
		mSrcPictureView.setImageBitmap(mBitmap);
		
		mSrcPictureView.post(new Runnable() {
			@Override
			public void run() {  
                init(); 
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.ok_menu, menu);
		return true;
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageView view = (ImageView) v;
		//多点触控
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:
				mSavedMatrix.set(mCurrentMatrix);
				//設置初始點位置
				mStart.set(event.getX(), event.getY());
				mCurrentMode = DRAG;
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				mOldDistance = distance(event);
				if (mOldDistance > 10f) {
					mSavedMatrix.set(mCurrentMatrix);
					setMidPoint(mMid, event);
					mCurrentMode = ZOOM;
				}
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_POINTER_UP:
				mCurrentMode = NONE;
				break;
			case MotionEvent.ACTION_MOVE:
				if (mCurrentMode == DRAG) {
					mCurrentMatrix.set(mSavedMatrix);
					mCurrentMatrix.postTranslate(event.getX() - mStart.x, event.getY()
							- mStart.y);
				} else if (mCurrentMode == ZOOM) {
					float newDistance = distance(event);
					if (newDistance > 10f) {
						mCurrentMatrix.set(mSavedMatrix);
						float scale = newDistance / mOldDistance;
						mCurrentMatrix.postScale(scale, scale, mMid.x, mMid.y);
					}
				}
				break;
			}

		view.setImageMatrix(mCurrentMatrix);
		return true; 
	}
	
	//计算两点之间的距离
	private float distance(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return (float)Math.sqrt(x * x + y * y);
	}

	//设置两个点的中点
	private void setMidPoint(PointF point, MotionEvent event) {
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.action_ok:
			clipPicture();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	// 等比例压缩图片
	private Bitmap getBitmap(Bitmap bitmap, int screenWidth, int screenHight) {
		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		Matrix matrix = new Matrix();
		float scale = (float) screenWidth / w;
		float scale2 = (float) screenHight / h;

		scale = scale > scale2 ? scale : scale2;

		// 保证图片不变形.
		matrix.postScale(scale, scale);
		// w,h是原图的属性.
		return Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
	}
	
	private void clipPicture() {
		Bitmap bitmap = getBitmap();
		
		String path = getExternalCacheDir() + "/head.jpg";
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(path);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
		
		EventBus.getDefault().post(new Event(path));  
		finish();
	}

	//获取矩形区域内的截图
	private Bitmap getBitmap() {
		Bitmap screenShoot = takeScreenShot();
		
		mClipView = (ClipView)this.findViewById(R.id.clip_view);
		Bitmap finalBitmap = Bitmap.createBitmap(screenShoot,
				mClipView.getSpaceX(), mClipView.getSpaceY() + mActionBarHeight + mStatusBarHeight, 
				mClipView.getBoundWidth(), mClipView.getBoundHeight());
		return finalBitmap;
	}
		
	@SuppressWarnings("deprecation")
	private void init() {
		//获取状态栏高度
		Rect frame = new Rect();
		this.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
		mStatusBarHeight = frame.top;
		
		View v = getWindow().findViewById(Window.ID_ANDROID_CONTENT);//获得根视图
		mActionBarHeight = v.getTop() - mStatusBarHeight;
		
		WindowManager manager = getWindowManager();
		mWindowWidth = manager.getDefaultDisplay().getWidth();
		mWindowHeight = manager.getDefaultDisplay().getHeight() - mActionBarHeight - mStatusBarHeight;
		//window_height = v.getHeight();
		
		int spaceX = (mWindowWidth - mBitmap.getWidth()) / 2;
		int spaceY = (mWindowHeight - mBitmap.getHeight()) / 2;
		
		mCurrentMatrix.setTranslate(spaceX, spaceY);
		mSrcPictureView.setImageMatrix(mCurrentMatrix);
	}
		
	//获取Activity的截屏
	private Bitmap takeScreenShot() {
		View view = this.getWindow().getDecorView();
		view.setDrawingCacheEnabled(true);
		view.buildDrawingCache();
		return view.getDrawingCache();
	}
}
