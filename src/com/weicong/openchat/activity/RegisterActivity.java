package com.weicong.openchat.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.appkefu.lib.interfaces.KFIMInterfaces;
import com.weicong.openchat.R;
import com.weicong.openchat.utils.ToolUtil;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class RegisterActivity extends Activity {

	private EditText mUsernameText;
	private EditText mPasswordText;
	private EditText mPasswordConfirmText;
	
	private ProgressDialog mProgressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		mProgressDialog = ToolUtil.showProgressDialog(this, "正在注册...");
		
		mUsernameText = (EditText) findViewById(R.id.et_username);
		mPasswordText = (EditText) findViewById(R.id.et_password);
		mPasswordConfirmText = (EditText) findViewById(R.id.et_password_confirm);
		
		Button registerButton = (Button) findViewById(R.id.btn_register);
		registerButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				validate();
			}
		});
		ToolUtil.pushActivity(this);
	}
	
	
	@Override
	protected void onDestroy() {
		Crouton.clearCroutonsForActivity(this);
		ToolUtil.removeActivity(this);
		super.onDestroy();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void validate() {
		String username = mUsernameText.getText().toString();
		String password = mPasswordText.getText().toString();
		String passConfirm = mPasswordConfirmText.getText().toString();
		
		if (username.equals("")) {
			mUsernameText.setError("请输入用户名");
			return;
		}
		
		if (password.equals("")) {
			mPasswordText.setError("请输入密码");
			return;
		}
		
		if (passConfirm.equals("")) {
			mPasswordConfirmText.setError("请输入确认密码");
			return;
		}
		
		if (username.length() < 6) {
			mUsernameText.setError("少于6位数");
			return;
		}
		
		if (password.length() < 6) {
			mPasswordText.setError("少于6位数");
			return;
		}
		
		if (passConfirm.length() < 6) {
			mPasswordConfirmText.setError("少于6位数");
			return;
		}
		
		if (!password.equals(passConfirm)) {
			mPasswordConfirmText.setError("密码不一致");
			return;
		}
		
		register(username, password);
	}
	
	/**
	 * 注册
	 */
	private void register(String username, String password) {
		mProgressDialog.show();
		new RegisterTask().execute(username, password);
		hideSoftInputView();
	}
	
	/**
     * 隐藏软键盘
     */
    private void hideSoftInputView() {
		InputMethodManager manager = ((InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE));
		if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
			if (getCurrentFocus() != null)
				manager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}
    
	private class RegisterTask extends AsyncTask<String, Void, Integer> {
		
		private String username;
		private String password;
		
		@Override
		protected Integer doInBackground(String... s) {
			username = s[0];
			password = s[1];
			return KFIMInterfaces.register(s[0], s[1]);
		}
		
		@Override
		protected void onPostExecute(Integer result) {
			mProgressDialog.dismiss();
			if(result == 1) { //注册成功
				KFIMInterfaces.login(RegisterActivity.this, username, password);
				Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
		    	startActivity(intent);
		    	ToolUtil.clearActivitys(RegisterActivity.this);
        	} else if(result == -400001) { //注册失败:用户名长度最少为6(错误码:-400001)
        		
        		Crouton.makeText(RegisterActivity.this, "用户名长度最少为6", Style.CONFIRM).show();
        	} else if(result == -400002) { //注册失败:密码长度最少为6(错误码:-400002)
        		
        		Crouton.makeText(RegisterActivity.this, "密码长度最少为6", Style.CONFIRM).show();
        	} else if(result == -400003) { //注册失败:此用户名已经被注册(错误码:-400003)
        		
        		Crouton.makeText(RegisterActivity.this, "此用户名已经被注册", Style.CONFIRM).show();
        	} else if(result == -400004) { //注册失败:用户名含有非法字符(错误码:-400004)
        		
        		Crouton.makeText(RegisterActivity.this, "用户名含有非法字符", Style.CONFIRM).show();
        	} else if(result == 0) { //注册失败:其他原因
        		
        		//其他原因：有可能是短时间内重复注册，为防止恶意注册，服务器对同一个IP注册做了时间间隔限制，即10分钟内同一个IP只能注册一个账号
        		Crouton.makeText(RegisterActivity.this, "未知原因", Style.CONFIRM).show();
        	}
		}
	}
}
