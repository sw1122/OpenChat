package com.weicong.openchat.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONObject;


public class RobotUtil {
	
	private static final String API_KEY = "a662c2e9d24729f355ac6362314a97f5";
	
	private static final String URI = "http://www.tuling123.com/openapi/api?key=";
	
	private static final String CODE = "code";
	private static final String TEXT = "text";
	
	public static String getResponse(String text) {
		
		String info = null;
		try {
			info = URLEncoder.encode(text, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} 
		String getURL = URI + API_KEY + "&info=" + info; 
		
		HttpURLConnection connection = null;
		
		try {
			URL getUrl = new URL(getURL);
			connection = (HttpURLConnection) getUrl.openConnection();
			connection.connect();
			// 取得输入流，并使用Reader读取 
		    BufferedReader reader = new BufferedReader(new InputStreamReader( connection.getInputStream(), "utf-8"));
		    StringBuffer sb = new StringBuffer(); 
		    String line = ""; 
		    while ((line = reader.readLine()) != null) { 
		        sb.append(line); 
		    } 
		    reader.close();
		    return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				// 断开连接 
			    connection.disconnect();
			}
		}
		return null;
	}
	
	public static String getText(String response) {
		StringBuilder sb = new StringBuilder();
		
		try {
			JSONObject jsonObject = new JSONObject(response);
			sb.append(jsonObject.getString(TEXT));
			switch (jsonObject.getInt(CODE)) {
			case 100000: //文本类数据
				break;
			case 200000: //网址类数据
				sb.append("\n");
				sb.append(jsonObject.getString("url"));
				break;
			case 305000: //列车
			case 302000: //新闻
			case 308000: //菜谱、视频、小说
				sb.append("\n");
				sb.append(jsonObject.getJSONArray("list").getJSONObject(0).getString("detailurl"));
				break;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return sb.toString();
	}
}
