package com.weicong.openchat.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

import com.weicong.openchat.application.MyApplication;

public class ToolUtil {

	/**
	 * 初始化进度对话框
	 * @param context
	 * @return ProgressDialog对象
	 */
	public static ProgressDialog showProgressDialog(Context context, String message)
	{
		ProgressDialog progressDialog = new ProgressDialog(context); 
		progressDialog.setMessage(message);
		progressDialog.setCancelable(false);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setIndeterminate(true);
		return progressDialog;
	}
	
	/**
	 * 将activity压入application里的list中
	 * @param activity
	 */
	public static void pushActivity(Activity activity) {
		MyApplication app = (MyApplication)activity.getApplication();
		app.pushActivity(activity);
	}
	
	/**
	 * 清除所有的activity
	 * @param activity
	 */
	public static void clearActivitys(Activity activity) {
		MyApplication app = (MyApplication)activity.getApplication();
		app.clearActivitys();
	}
	
	/**
	 * 清除当前的activity
	 * @param activity
	 */
	public static void removeActivity(Activity activity) {
		MyApplication app = (MyApplication)activity.getApplication();
		app.removeActivity(activity);
	}
}
